#!/bin/bash
inputFile=$1
outputFile=$2
header="uuid,trial_type,is_practice,trial_date_time,trial_number,RT,response,confidence,ink_color,word,session_id,has_hint"

echo $header > $outputFile
grep "INSERT INTO \`speech_trial" < $inputFile >> $outputFile
sed -i 's/),(/\n/g' $outputFile
sed -i 's/INSERT INTO `speech_trial` VALUES (//g' $outputFile
sed -i "s/'//g" $outputFile
sed -i "s/);$//" $outputFile
