Speechstroop
============

An online stroop task that uses speech recognition. Backend runs in Django (originally hosted on PythonAnywhere).

Requires:
- Python 3.5.9
- Django 2.2.7
- mysqlclient 1.4.4
