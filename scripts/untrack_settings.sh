#!/bin/bash

# Run this script to stop Git from tracking speechstroop/settings.py. This is
# desireable because settings.py contains database settings, but I'm having
# trouble getting local development environments to work with MySQL. Instead,
# using SQlite.

git update-index --assume-unchanged ../speechstroop/settings.py
