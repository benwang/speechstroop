// Detect browser
var isChrome = !!window.chrome;// && (!!window.chrome.webstore || !!window.chrome.runtime);
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isFirefox = typeof InstallTrigger !== 'undefined';
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
var isIE = /*@cc_on!@*/false || !!document.documentMode;
var isEdge = !isIE && !!window.StyleMedia;

console.log("Chrome:" + isChrome + " Opera:" + isOpera + " isFirefox:" + isFirefox + " Safari:" + isSafari + " IE:" + isIE + " Edge:" + isEdge);

if (isChrome && !isOpera && !isIE && !isEdge && !isSafari && !isOpera) {
  console.log("Google Chrome detected!");

  // Detect Microphone
  // navigator.mediaDevices.getUserMedia({audio : true}, micSuccess, micFail);

} else {
  // Reformat document
  document.getElementById("startButton").disabled = true;
  document.getElementById("browserRequirement").style.color = "red";
  document.getElementById("browserRequirementDetail").style.display = "block";

  // Display alert
  alert("Sorry, but Google Chrome is required to perform this task. Please try again with a different web browser.");
}

function micSuccess(stream) {
  console.log("Success. Microphone found");
}

function micFail(stream) {
  micFail();
}

function micFail() {
  // Suppress speech not recognized prompt
  speechRecognized = true;

  document.getElementById("micRequirement").style.color = "red";
  console.log("Error: Microphone not found");

  alert("A microphone was not detected. A microphone is required for this task. Please connect a microphone and refresh the page.");
}

function micFailPermission() {
  // Suppress speech not recognized prompt
  speechRecognized = true;

  document.getElementById("micRequirement").style.color = "red";
  document.getElementById("micRequirementDetail").style.display = "block";
  console.log("Error: Microphone permission denied");

  alert("Microphone permission was not granted. Please allow microphone use and refresh the page.");
}

//////////////////////////////////////////////////////////
///////// Speech Recognition Section /////////////////////
//////////////////////////////////////////////////////////
var TIMEOUT_MAX_MS_PER_WORD = 15000; // Max time to display each word
var TIMEOUT_MAX_MS_RECOGNITION = 1000; // Max time for processing

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

var colors = ['RED', 'BLUE', 'GREEN', 'YELLOW', 'BAD', 'POOR', 'LEGAL', 'DEEP'];
var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//recognition.continuous = false;
recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

// Variables for tracking recognition state
var currentTimeout;
var speechRecognized = false;

function onButtonClick() {
    // Start Recognition
    speechRecognized = false;
    recognition.start();

    // Set timeout for speech recognition processing
    currentTimeout = window.setTimeout(onError, TIMEOUT_MAX_MS_PER_WORD, 0, "ERROR_TIMEOUT_MAX_MS_PER_WORD", 0);
}

recognition.onresult = function(event) {
  clearTimeout(currentTimeout);
  speechRecognized = true;
  var transcript = event.results[0][0].transcript;
  // Move on if "Continue" was said
  if (transcript.localeCompare("continue") == 0) {
    window.location.href = "informedConsentDocument";
  }
  else {
    alert("Audio was recognized. You said \"" + transcript + "\". Please click again and say \"Continue\" to move on to the next page.");
  }
}

recognition.onspeechend = function() {
  console.log('onspeechend() called');
  recognition.stop();

  // Set timeout for speech recognition processing
  window.setTimeout(onError, TIMEOUT_MAX_MS_RECOGNITION, 0, "ERROR_TIMEOUT_MAX_MS_RECOGNITION", 0);
}

recognition.onnomatch = function(event) {
  console.log('onnomatch() called: ' + event.error);
  clearTimeout(currentTimeout);
  onError();
}

recognition.onerror = function(event) {
  console.log('onerror() called: ' + event.error);
  clearTimeout(currentTimeout);

  // Check for failure reason
  if (event.error == 'not-allowed') {
    // Microphone permission not granted
    micFailPermission();
  } else if (event.error == 'no-speech') {
    onError();
  } else {
    onError();
  }
}

onError = function() {
  if (!speechRecognized) {
    recognition.stop();

    alert("No speech was detected. Please click the green button again and say the word \"Continue\". If this is repeatedly failing, make sure your microphone is plugged in and working, otherwise you will not be able to complete this task. Also be sure to Allow Microphone when prompted.");

    document.getElementById("micRequirement").style.color = "red";

    speechRecognized = true;
  }
}
