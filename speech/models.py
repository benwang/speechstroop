import datetime

from django.db import models
from django.utils import timezone
from datetime import datetime

class StroopSession(models.Model):
    MALE='MALE'
    FEMALE='FEMALE'
    OTHER='OTHER'
    GENDER_CHOICES=[
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (OTHER, 'Other')
    ]

    RIGHT='RIGHT'
    LEFT='LEFT'
    AMBIDEXTROUS='AMBIDEXTROUS'
    DOMINANT_HAND_CHOICES=[
        (RIGHT, 'Right'),
        (LEFT, 'Left'),
        (AMBIDEXTROUS, 'Ambidextrous')
    ]

    MORNING='MORNING'
    AFTERNOON='AFTERNOON'
    EVENING='EVENING'
    NO_DIFFERENCES='NO_DIFFERENCES'
    TIME_OF_DAY_CHOICES=[
        (MORNING, 'Morning'),
        (AFTERNOON, 'Afternoon'),
        (EVENING, 'Evening'),
        (NO_DIFFERENCES, 'No differences')
    ]

    AMERICAN_INDIAN_ALASKAN_NATIVE='AMERICAN_INDIAN_ALASKAN_NATIVE'
    ASIAN='ASIAN'
    NATIVE_HAWAIIAN_PACIFIC_ISLANDER='NATIVE_HAWAIIAN_PACIFIC_ISLANDER'
    BLACK_AFRICAN_AMERICAN='BLACK_AFRICAN_AMERICAN'
    WHITE_CAUCASIAN='WHITE_CAUCASIAN'
    MORE_THAN_ONE_RACE='MORE_THAN_ONE_RACE'
    PREFER_NOT_TO_RESPOND='PREFER_NOT_TO_RESPOND'
    RACIAL_CATEGORY_CHOICES=[
        (AMERICAN_INDIAN_ALASKAN_NATIVE, 'American Indian / Alaskan Native'),
        (ASIAN, 'Asian'),
        (NATIVE_HAWAIIAN_PACIFIC_ISLANDER, 'Native Hasaiian or Other Pacific Islander'),
        (BLACK_AFRICAN_AMERICAN, 'Black / African American'),
        (WHITE_CAUCASIAN, 'White / Caucasian'),
        (MORE_THAN_ONE_RACE, 'More than One Race'),
        (PREFER_NOT_TO_RESPOND, 'Prefer Not To Respond')
    ]

    YES='YES'
    NO='NO'
    UNSURE='UNSURE'
    YES_NO_PREFER_NOT_CHOICES=[
        (YES, 'Yes'),
        (NO, 'No'),
        (PREFER_NOT_TO_RESPOND, 'Prefer Not To Respond')
    ]
    YES_NO_UNSURE_CHOICES=[
        (YES, 'Yes'),
        (NO, 'No'),
        (UNSURE, 'Unsure')
    ]
    YES_NO_CHOICES=[
        (YES, 'Yes'),
        (NO, 'No')
    ]

    INCOME_UNDER_15K='INCOME_UNDER_15K'
    INCOME_15K_24999='INCOME_15K_24999'
    INCOME_25K_34999='INCOME_25K_34999'
    INCOME_35K_49999='INCOME_35K_49999'
    INCOME_50K_74999='INCOME_50K_74999'
    INCOME_75K_99999='INCOME_75K_99999'
    INCOME_100K_149999='INCOME_100K_149999'
    INCOME_150K_199999='INCOME_150K_199999'
    INCOME_200K_PLUS='INCOME_200K_PLUS'
    HOUSEHOLD_INCOME=[
        (INCOME_UNDER_15K, '$14,999 and under'),
        (INCOME_15K_24999, '$15,000 to $24,999'),
        (INCOME_25K_34999, '$25,000 to $34,999'),
        (INCOME_35K_49999, '$35,000 to $49,999'),
        (INCOME_50K_74999, '$50,000 to $74,999'),
        (INCOME_75K_99999, '$75,000 to $99,999'),
        (INCOME_100K_149999, '$100,000 to $149,999'),
        (INCOME_150K_199999, '$150,000 to $199,999'),
        (INCOME_200K_PLUS, '$200,000 and over')
    ]

    LAPTOP='LAPTOP'
    DESKTOP='DESKTOP'
    TABLET='TABLET'
    ALL_IN_ONE='ALL_IN_ONE'
    UNSURE_OTHER='UNSURE_OTHER'
    COMPUTER_TYPE=[
        (LAPTOP, 'Laptop'),
        (DESKTOP, 'Desktop'),
        (TABLET, 'Tablet'),
        (ALL_IN_ONE, 'All-in-one computer'),
        (UNSURE_OTHER, 'Unsure or other')
    ]

    MIC_BUILT_IN='MIC_BUILT_IN'
    MIC_EXTERNAL='MIC_EXTERNAL'
    MIC_HEADSET='MIC_HEADSET'
    MIC_TYPE=[
        (MIC_BUILT_IN, 'Built-in microphone'),
        (MIC_EXTERNAL, 'External (USB) microphone'),
        (MIC_HEADSET, 'Headset'),
        (UNSURE_OTHER, 'Unsure or other')
    ]

    HOURS_LESS_THAN_HOUR='HOURS_LESS_THAN_HOUR'
    HOURS_1_4='HOURS_1_4'
    HOURS_5_8='HOURS_5_8'
    HOURS_9_12='HOURS_9_12'
    HOURS_13_16='HOURS_13_16'
    HOURS_17_PLUS='HOURS_17_PLUS'
    COMPUTER_USAGE=[
        (HOURS_LESS_THAN_HOUR, 'Less than 1 hour'),
        (HOURS_1_4, '1 to 4 hours'),
        (HOURS_5_8, '5 to 8 hours'),
        (HOURS_9_12, '9 to 12 hours'),
        (HOURS_13_16, '13 to 16 hours'),
        (HOURS_17_PLUS, '17 hours or more')
    ]

    ########################################
    # Fields
    ########################################
    subject_id = models.CharField(
        max_length=20,
        primary_key=True,
    )

    # Prolific-specific IDs
    prolific_pid = models.CharField(
        max_length=32,
        default=''
    )
    study_id = models.CharField(
        max_length=32,
        default=''
    )
    session_id = models.CharField(
        max_length=32,
        default=''
    )

    # Trial information
    num_practice_trials = models.IntegerField(
        default=0
    )
    num_trials = models.IntegerField(
        default=0
    )
    num_trials_before_break = models.IntegerField(
        default=0
    )
    break_duration_sec = models.IntegerField(
        default=0
    )
    valid = models.BooleanField(
        default=False
    )
    # completion_code = models.CharField(
    #     max_length=10
    # )
    num_practice_sessions_completed = models.IntegerField(
        default=0
    )
    task_in_progress = models.BooleanField(
        default=False
    )

    # Informed Consent Form information
    informed_consent_signed = models.BooleanField(
        default=False
    )
    # informed_consent_signature = models.CharField(
    #     verbose_name="Type your name in the box to sign the informed consent form:",
    #     max_length=128,
    #     default='',
    # )
    informed_consent_signed_date_time = models.DateTimeField(
        default=datetime.min,
    )

    # Health and demographic information
    # birthdate = models.DateField(
    #     verbose_name="What is your birthdate?",
    #     default=datetime.min
    # )
    # gender = models.CharField(
    #     verbose_name="What is your gender?",
    #     max_length=10,
    #     choices=GENDER_CHOICES,
    #     default=OTHER
    # )
    age = models.IntegerField(
        verbose_name="What is your age?",
        default=0
    )
    years_of_education = models.IntegerField(
        verbose_name="How many years of formal education have you had (consider graduating high school to be 12 years)?",
        default=0
    )
    # dominant_hand = models.CharField(
    #     verbose_name="What is your dominant hand (the hand you write with)?",
    #     max_length=12,
    #     choices=DOMINANT_HAND_CHOICES,
    #     default=RIGHT
    # )
    # alert_time_of_day = models.CharField(
    #     verbose_name="Please indicate what time of day you feel most alert:",
    #     max_length=14,
    #     choices=TIME_OF_DAY_CHOICES,
    #     default=NO_DIFFERENCES
    # )
    racial_categories = models.CharField(
        verbose_name="Please select the racial category that apply to you:",
        max_length=32,
        choices=RACIAL_CATEGORY_CHOICES,
        default=PREFER_NOT_TO_RESPOND
    )
    is_latino = models.CharField(
        verbose_name="Do you consider yourself to be Hispanic or Latino?",
        max_length=21,
        choices=YES_NO_PREFER_NOT_CHOICES,
        default=NO
    )
    # is_english_first_language = models.CharField(
    #     verbose_name="Is English your first language?",
    #     max_length=3,
    #     choices=YES_NO_CHOICES,
    #     default=NO
    # )
    # first_language = models.CharField(
    #     verbose_name="If English is not your first language, what is your first language?",
    #     max_length=20,
    #     default="",
    #     blank=True
    # )
    # learn_english_before_age_5 = models.CharField(
    #     verbose_name="Did you learn English before the age of 5?",
    #     max_length=3,
    #     choices=YES_NO_CHOICES,
    #     default=NO
    # )
    # age_learned_english = models.IntegerField(
    #     verbose_name="If you did not learn English before the age of 5, at what age did you learn English?",
    #     default=0,
    #     blank=True
    # )
    household_income = models.CharField(
        verbose_name="What is your household income in USD?",
        default=INCOME_UNDER_15K,
        max_length=18,
        choices=HOUSEHOLD_INCOME
    )
    computer_os = models.CharField(
        verbose_name='Computer OS (autofilled)',
        default="",
        max_length=128,
        blank=True
    )
    computer_screen_width_px = models.IntegerField(
        verbose_name='Screen width (autofilled)',
        default=0,
        blank=True
    )
    computer_screen_height_px = models.IntegerField(
        verbose_name='Screen height (autofilled)',
        default=0,
        blank=True
    )
    computer_type = models.CharField(
        verbose_name='Please describe the computer you are on',
        default=UNSURE_OTHER,
        max_length=12,
        choices=COMPUTER_TYPE
    )
    microphone_type = models.CharField(
        verbose_name='Please describe your computer\'s microphone',
        default=UNSURE_OTHER,
        max_length=12,
        choices=MIC_TYPE
    )
    hours_on_current_computer = models.CharField(
        verbose_name='Within the past week how many hours have you used the computer you are currently on?',
        default=HOURS_LESS_THAN_HOUR,
        max_length=20,
        choices=COMPUTER_USAGE
    )
    additional_comments = models.CharField(
        verbose_name="Is there anything we should know about that might affect your performance during the testing session today (eg., lack of sleep, feeling ill, etc.)?",
        default="",
        max_length=255,
        blank=True
    )

    # Space for comments on how the task went afterwards (explain if there were any problems, etc)
    post_task_comments = models.CharField(
        verbose_name="Do you have any comments for us on how your session went today?",
        max_length=255,
        default="",
        blank=True
    )

    done_stroop_previously = models.CharField(
        verbose_name="Have you ever done the Stroop task before doing today's task (i.e., on Lumosity, in another online study, at a University, in a laboratory experiment, etc.)?",
        max_length=11,
        default=UNSURE,
        choices=YES_NO_UNSURE_CHOICES
    )

    def __str__(self):
        # return "id:" + self.subject_id + ",num_practice_trials:" + str(self.num_practice_trials) + ",num_trials:" + str(self.num_trials) + ",valid:" + str(self.valid) + ", birthdate:" + str(self.birthdate) + ",gender:" + str(self.gender) + ",years_of_education:" + str(self.years_of_education) + ",dominant_hand:" + str(self.dominant_hand) + ",alert_time_of_day:" + str(self.alert_time_of_day) + ",racial_categories:" + str(self.racial_categories) + ",is_latino:" + str(self.is_latino) + ",is_english_first_language:" + str(self.is_english_first_language) + ",first_language:" + str(self.first_language) + ",learn_english_before_age_5:" + str(self.learn_english_before_age_5) + ",age_learned_english:" + str(self.age_learned_english) + ",additional_comments:" + str(self.additional_comments) + ",post_task_comments:" + str(self.post_task_comments)
        # return "id:" + self.subject_id + ",num_practice_trials:" + str(self.num_practice_trials) + ",prolific_pid:" + str(self.prolific_pid) + ",study_id:" + str(self.study_id) + ",session_id:" + str(self.session_id) + ",num_trials:" + str(self.num_trials) + ",valid:" + str(self.valid) + ",years_of_education:" + str(self.years_of_education) + ",racial_categories:" + str(self.racial_categories) + ",is_latino:" + str(self.is_latino) + ",additional_comments:" + str(self.additional_comments) + ",post_task_comments:" + str(self.post_task_comments)
        return "subject_id:" + str(self.subject_id) + ",prolific_pid:" + str(self.prolific_pid) + ",study_id:" + str(self.study_id) + ",session_id:" + str(self.session_id) + ",num_practice_trials:" + str(self.num_practice_trials) + ",num_trials:" + str(self.num_trials) + ",num_trials_before_break:" + str(self.num_trials_before_break) + ",break_duration_sec:" + str(self.break_duration_sec) + ",valid:" + str(self.valid) + ",num_practice_sessions_completed:" + str(self.num_practice_sessions_completed) + ",task_in_progress:" + str(self.task_in_progress) + ",informed_consent_signed:" + str(self.informed_consent_signed) + ",informed_consent_signed_date_time:" + str(self.informed_consent_signed_date_time) + ",age:" + str(self.age) + ",years_of_education:" + str(self.years_of_education) + ",racial_categories:" + str(self.racial_categories) + ",is_latino:" + str(self.is_latino) + ",household_income:" + str(self.household_income) + ",computer_os:" + str(self.computer_os) + ",computer_screen_width_px:" + str(self.computer_screen_width_px) + ",computer_screen_height_px:" + str(self.computer_screen_height_px) + ",computer_type:" + str(self.computer_type) + ",microphone_type:" + str(self.microphone_type) + ",hours_on_current_computer:" + str(self.hours_on_current_computer) + ",additional_comments:" + str(self.additional_comments) + ",post_task_comments:" + str(self.post_task_comments)

# Create your models here.
class Trial(models.Model):
    CONGRUENT = 'CN'
    INCONGRUENT = 'IN'
    NEUTRAL = 'NU'
    TRIAL_TYPE_CHOICES = [
        (CONGRUENT, 'CONGRUENT'),
        (INCONGRUENT, 'INCONGRUENT'),
        (NEUTRAL, 'NEUTRAL'),
    ]

    # Colors
    RED = 'RED'
    BLUE = 'BLUE'
    GREEN = 'GREEN'
    YELLOW = 'YELLOW'

    # Words
    BAD = 'BAD'
    POOR = 'POOR'
    LEGAL = 'LEGAL'
    DEEP = 'DEEP'

    COLOR_CHOICES = [
        (RED, 'RED'),
        (BLUE, 'BLUE'),
        (GREEN, 'GREEN'),
        (YELLOW, 'YELLOW'),
    ]

    WORD_CHOICES = [
        (RED, 'RED'),
        (BLUE, 'BLUE'),
        (GREEN, 'GREEN'),
        (YELLOW, 'YELLOW'),
        (BAD, 'BAD'),
        (POOR, 'POOR'),
        (LEGAL, 'LEGAL'),
        (DEEP, 'DEEP'),
    ]

    ########################################
    # Fields
    ########################################
    # UUID = session_id + trial_number
    uuid = models.CharField(
        max_length=(10 + 4),
        primary_key=True,
        default='',
    )
    session = models.ForeignKey(StroopSession, on_delete=models.CASCADE)
    trial_type = models.CharField(
        max_length=10,
        choices=TRIAL_TYPE_CHOICES,
        default=NEUTRAL,
    ) # congruent, incongruent, or neutral
    is_practice = models.BooleanField(
        default=False,
    )
    has_hint = models.BooleanField(
        default=False,
    )
    trial_date_time = models.DateTimeField(
        default=datetime.min,
    )
    trial_number = models.IntegerField(
        default=0,
    )
    RT = models.IntegerField(
        default=0,
    )
    response = models.CharField(
        max_length=128,
        default=''
    )
    confidence = models.DecimalField(
        max_digits=8,
        decimal_places=6,
        default=0.0,
    )
    ink_color = models.CharField(
        max_length=10,
        choices=COLOR_CHOICES,
        default=RED
    )
    word = models.CharField(
        max_length=10,
        choices=WORD_CHOICES,
        default=RED
    )

    def __str__(self):
        return "id:" + str(self.session) + ",type:" + self.trial_type + ",datetime:" + str(self.trial_date_time) + ",num:" + str(self.trial_number) + ",RT:" + str(self.RT) + "resp:" + self.response + ",conf:" + str(self.confidence) + ",ink:" + self.ink_color + ",word:" + self.word
