#!/bin/bash
inputFile=$1
outputFile=$2
# header="subject_id,num_practice_trials,num_trials,valid,completion_code,additional_comments,age_learned_english,alert_time_of_day,birthdate,dominant_hand,first_language,gender,is_english_first_language,is_latino,learn_english_before_age_5,post_task_comments,racial_categories,years_of_education,informed_consent_signed,informed_consent_signed_date_time,num_practice_sessions_com  pleted,task_in_progress"
header="subject_id,num_practice_trials,num_trials,valid,additional_comments,informed_consent_signed,informed_consent_signed_date_time,is_latino,num_practice_sessions_completed,post_task_comments,racial_categories,task_in_progress,years_of_education,prolific_pid,session_id,study_id,computer_os,computer_screen_height_px,ccomputer_screen_width_px,computer_type,hours_on_current_computer,household_income,microphone_type,break_duration_sec,num_trials_before_break,age,done_stroop_previously"

echo $header > $outputFile
grep "INSERT INTO \`speech_stroopsession" < $inputFile >> $outputFile
sed -i 's/\\"/*/g' $outputFile
sed -i 's/'\'',/",/g' $outputFile
sed -i 's/,'\''/,"/g' $outputFile
sed -i 's/('\''/("/g' $outputFile
sed -i 's/'\'')/")/g' $outputFile
sed -i 's/),(/\n/g' $outputFile
sed -i 's/INSERT INTO `speech_stroopsession` VALUES (//g' $outputFile
sed -i "s/'//g" $outputFile
sed -i "s/);$//" $outputFile
