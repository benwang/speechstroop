from django.urls import path

from . import views

app_name = 'speech'
urlpatterns = [
    path('', views.index, name='index'),
    path('informedConsentDocument/', views.informedConsentDocument, name='informedConsentDocument'),
    path('healthAndDemographicsForm/', views.healthAndDemographicsForm, name='healthAndDemographicsForm'),
    path('practice/', views.practice, name='practice'),
    path('initialize/', views.initialize, name='initialize'),
    path('practiceCompleteAck/', views.practiceCompleteAck, name='practiceCompleteAck'),
    path('practiceRetry/', views.practiceRetry, name='practiceRetry'),
    path('practiceComplete/', views.practiceComplete, name='practiceComplete'),
    path('task/', views.task, name='task'),
    path('taskComplete/', views.taskComplete, name='taskComplete'),
    path('postTaskSurvey/', views.postTaskSurvey, name='postTaskSurvey'),
    path('success/', views.success, name='success'),
    path('error/', views.error, name='error'),
]
