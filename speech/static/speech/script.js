// Parameters
var DURATION_FIXATION_CROSS = 700;
var DURATION_BLANK = 50;
var TIMEOUT_MAX_MS_PER_WORD = 5000; // Max time to display each word
var TIMEOUT_MAX_MS_RECOGNITION = 1000; // Max time for processing

// Time to wait before refreshing the page (longer for practice rounds)
var DURATION_REFRESH_WAIT = 50;
var DURATION_PRACTICE_REFRESH_WAIT = 2000;

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

const INK_COLOR = 0;
const WORD = 1;
const IS_PRACTICE = 2;
const HAS_HINT = 3;

var colors = ['RED', 'BLUE', 'GREEN', 'YELLOW', 'BAD', 'POOR', 'LEGAL', 'DEEP'];
var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

// Manually replaced words
var REPLACEMENT_WORDS = [
  ['BREAD', 'FRED', 'RUN', 'BAD', 'BED', 'BRAD', 'DEAD', 'HEAD', 'RAD', 'RENT', 'WEB'], // red
  ['BLOW', 'SLU', '2', 'BLOOM', 'FLU', 'GLUE', 'LIU', 'POO'], // blue
  ['BRAIN', 'CREAM', 'GREED', 'GREAT', 'SCREAM', 'STREAM', '18', 'GRADE', 'GREETING', 'RAIN', 'RING', 'SCREEN'], // green
  ['HELLO', 'JELL-O', 'YELLA', 'ZILLOW', 'YO'] // yellow
];

var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//recognition.continuous = false;
recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

var bg = document.querySelector('html');
var diagnostic = document.querySelector('.output');
var diagnosticHandled = false;

// Time variables
var startTime;

// Trial variables
var word;
var inkColor;
var isPractice;
var hasHint;
var fontColor;

var prevWasPractice = 0;

// Timeouts
var startTrialTimeout;
var onSpeechEndTimeout;


var goFS = document.getElementById("goFS");
goFS.addEventListener("click", function() {
    goFS.style.display = "none";
    document.body.requestFullscreen();
    console.log("Starting with trial number " + trialNum);
    init();
}, false);

// Variables for handling the mid-session break
var breakCompleted = false;
var breakText = document.getElementById("breakText");
var endBreakButton = document.getElementById("endBreakButton");
endBreakButton.addEventListener("click", function() {
    endBreakButton.style.display = "none";
    breakText.style.display = "none";
    // console.log("Starting with trial number " + trialNum);
    breakCompleted = true;
    init();
}, false);

// Update countdown for break
function updateCountdown()
{
  // endBreakButton.innerText = "Continue (" + countdown + ")";
  endBreakButton.innerText = countdown;
  console.log(endBreakButton.value);
  countdown -= 1;
  if (countdown >= 0)
  {
    window.setTimeout(updateCountdown, 1000);
  }
  else
  {
    enableBreakButton();
  }
}

// Enable button to continue from break
function enableBreakButton()
{
  endBreakButton.disabled = false;
  // endBreakButton.innerText = "Continue";
  endBreakButton.innerText = "Please click here to resume the task";
}

function init() {
  // Check for break
  if (trialNum == breakTrialNum && !breakCompleted)
  {
    // Hide task-related elements
    document.getElementById("cross").style.display = "none";
    document.getElementById("text").style.display = "none";
    document.getElementById("output").style.display = "none";

    // Show break-related elements
    breakText.style.display = "block";
    endBreakButton.disabled = true;
    endBreakButton.style.display = "block";

    // Start countdown timer
    updateCountdown();
  }
  else
  {
    // Grab trial
    word = trialList[trialNum][WORD];
    inkColor = trialList[trialNum][INK_COLOR];
    isPractice = trialList[trialNum][IS_PRACTICE];
    hasHint = trialList[trialNum][HAS_HINT];

    if (inkColor == Color.BLUE) {
      fontColor = "DodgerBlue";
    } else {
      fontColor = Color[inkColor];
    }

    //====================  DEBUG ONLY =======================
    // console.log("word has been assigned to " + word);
    // console.log("inkColor has been assigned to " + inkColor);
    // console.log("isPractice has been assigned to " + isPractice);
    // console.log("hasHint has been assigned to " + hasHint);
    // console.log("fontColor has been assigned to " + fontColor);
    //====================  DEBUG ONLY =======================

    // Reset html
    document.getElementById("cross").style.display = "block";
    document.getElementById("text").style.display = "none";
    document.getElementById("text").style.color = fontColor;
    document.getElementById("text").innerHTML = "<b>" + word + "</b>";

    diagnostic.textContent = "";
    if (hasHint) {
      document.getElementById("output").style.display = "block";
    } else {
      document.getElementById("output").style.display = "none";
    }

    // Start task after 1500 ms fixation cross
    window.setTimeout(startBlank, DURATION_FIXATION_CROSS);
  }
}

function startBlank() {
  document.getElementById("cross").style.display = "none";
  window.setTimeout(startTrial, DURATION_BLANK);
}

function startTrial() {
  // Set visibility of text
  document.getElementById("text").style.display = "block";

  // Start listening
  startTime = Date.now();
  recognition.start();
  diagnosticHandled = false;
  console.log('Ready to receive a color command.');

  // Set timeout for speech recognition processing
  startTrialTimeout = window.setTimeout(postResults, TIMEOUT_MAX_MS_PER_WORD, 0, "ERROR_TIMEOUT_MAX_MS_PER_WORD", 0);
}

recognition.onspeechstart = function() {
  endTime = Date.now();
  console.log('onspeechstart() called');
}

recognition.onresult = function(event) {
  var last = event.results.length - 1;
  var color = event.results[last][0].transcript;

  bg.style.backgroundColor = color;
  console.log('Confidence: ' + event.results[0][0].confidence);

  postResults(endTime - startTime, event.results[last][0].transcript, event.results[0][0].confidence);
  console.log('Successful recognition. POST success!');
}

recognition.onspeechend = function() {
  console.log('onspeechend() called');
  recognition.stop();

  // Set timeout for speech recognition processing
  onSpeechEndTimeout = window.setTimeout(postResults, TIMEOUT_MAX_MS_RECOGNITION, 0, "ERROR_TIMEOUT_MAX_MS_RECOGNITION", 0);
}

recognition.onnomatch = function(event) {
  postResults(0, "ERROR_ON_NO_MATCH", 0);
  console.log('onnomatch() POST complete!');
}

recognition.onerror = function(event) {
  console.log('onerror() called');

  postResults(0, "ERROR_ON_ERROR", 0);
  console.log('onerror() POST complete!');
}

function postResults(rt, response, confidence) {
  recognition.stop();

  if (!diagnosticHandled)
  {
    // Flag as handled
    diagnosticHandled = true;

    // Manually filter for misheard words
    response = filterResponse(response.toUpperCase());
    console.log('postResults() response after filtering: ' + response);

    $.post('/speech/taskComplete/', {"trialNum": trialNum, "RT": rt, "response": response, "confidence": confidence});
    console.log('postResults() POST complete!');

    diagnostics(response);
  }
}

function filterResponse(response) {
  for (i = 0; i < REPLACEMENT_WORDS.length; i++) {
    if (REPLACEMENT_WORDS[i].includes(response)) {
      console.log('filterResponse(' + response + ') Match found! Replacing with ' + colors[i]);
      return colors[i];
    }
  }

  return response;
}

function nextTrial() {
  // window.location.reload();
  // Cancel timeouts
  clearTimeout(startTrialTimeout);
  clearTimeout(onSpeechEndTimeout);

  // Incrememnt trial number
  trialNum++;
  console.log("trialNum incremented to " + trialNum)

  // Exit if end of practice or end of trials
  if (trialNum == trialList.length) {
    console.log("End of trials. Should redirect to success")
    window.location.reload();
  }

  if (prevWasPractice && !trialList[trialNum][IS_PRACTICE]) {
    console.log("End of practice trials. Should redirect to practice complete.")
    window.location.reload();
  }

  // Save whether previous was practice
  prevWasPractice = isPractice;

  // Re-initialize html
  init();
}
