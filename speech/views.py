from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from speech.models import Trial, StroopSession
from django.utils import timezone
import pytz
from random import shuffle, randint
from datetime import datetime
from django.conf import settings
from django.core.mail import send_mail

from .forms import HealthAndDemographicsForm, HealthAndDemographicsFormWithAge, PostTaskForm, InformedConsentDocumentForm

########################################
############## Parameters ##############
########################################
# Trial Generation Parameters
NUM_PRACTICE_CONGRUENT = 6
NUM_PRACTICE_INCONGRUENT = 6
NUM_PRACTICE_NEUTRAL = 6

NUM_PRACTICE_WITH_HINTS = 9

NUM_CONGRUENT = 54
NUM_INCONGRUENT = 54
NUM_NEUTRAL = 48

# Put the break in the middle of the trials
NUM_PRACTICE_TRIALS = NUM_PRACTICE_CONGRUENT + NUM_PRACTICE_INCONGRUENT + NUM_PRACTICE_NEUTRAL
NUM_TRIALS_BEFORE_BREAK = NUM_PRACTICE_TRIALS + (NUM_CONGRUENT + NUM_INCONGRUENT + NUM_NEUTRAL) / 2

# Duration of break
BREAK_DURATION_SEC = 30

# Duration for reading informed consent document (seconds)
INFORMED_CONSENT_DOCUMENT_READ_DURATION_SEC = 10

# Prefix for all completion codes
# Completion codes are "<Prefix><Subject Number, no padding>"
COMPLETION_CODE_PREFIX = "F7J"

# Completion Link
PROLIFIC_COMPLETION_LINK = "https://app.prolific.co/submissions/complete?cc=5EA9939D"
SUBJECT_POOL_COMPLETION_LINK = "https://wupsych.sona-systems.com/webstudy_credit.aspx?experiment_id=285&credit_token=35945ffab5b04fcb996627b0a48d007c&survey_code="
    # Fill out survey_code with the "PROLIFIC_PID" in https://wupsych.sona-systems.com/webstudy_credit.aspx?experiment_id=285&credit_token=35945ffab5b04fcb996627b0a48d007c&survey_code=XXXX

# Maximum number of digits for subject number (5 means up to 99,999)
MAX_NUM_SUBJECT_DIGITS = 5 # Up to 99,999 subjects
MAX_NUM_TRIAL_DIGITS = 3   # Up to 999 trials

# Practice success criteria
PRACTICE_MIN_ACCURACY_THRESHOLD = 0.75

# Debug print enable
DEBUG_NONE = 0
DEBUG_LOW = 1
DEBUG_MEDIUM = 2
DEBUG_HIGH = 3

DEBUG_LEVEL = DEBUG_MEDIUM

# Debug mode switch (shortens number of trials and timers)
if settings.DEBUG:
    # OPTION to turn debug mode on/off in development
    DEBUG = True
    # DEBUG = False
else:
    # Force debug mode off on deployment
    ## DO NOT CHANGE ##
    DEBUG = False
    ## DO NOT CHANGE ##

print("views.py: settings.DEBUG set to " + str(settings.DEBUG) + " and DEBUG set to " + str(DEBUG))

############ TEST CODE ONLY #################
if (DEBUG):
    INFORMED_CONSENT_DOCUMENT_READ_DURATION_SEC = 5

    NUM_PRACTICE_CONGRUENT = 1
    NUM_PRACTICE_INCONGRUENT = 1
    NUM_PRACTICE_NEUTRAL = 1
    NUM_PRACTICE_WITH_HINTS = 1
    NUM_CONGRUENT = 2
    NUM_INCONGRUENT = 2
    NUM_NEUTRAL = 2
    NUM_PRACTICE_TRIALS = NUM_PRACTICE_CONGRUENT + NUM_PRACTICE_INCONGRUENT + NUM_PRACTICE_NEUTRAL
    NUM_TRIALS_BEFORE_BREAK = NUM_PRACTICE_TRIALS + (NUM_CONGRUENT + NUM_INCONGRUENT + NUM_NEUTRAL) / 2
    BREAK_DURATION_SEC = 5

########################################
########################################

# Constants for accessing trialArray data
TRIAL_TYPE = 0;
INK_COLOR = 1;
WORD = 2;

########################################
def index(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** index() called  ***")
    request.session['newSession'] = True
    request.session['PROLIFIC_PID'] = request.GET.get('PROLIFIC_PID', 'PROLIFIC_PID_MISSING')
    request.session['SESSION_ID'] = request.GET.get('SESSION_ID', 'SESSION_ID_MISSING')
    request.session['STUDY_ID'] = request.GET.get('STUDY_ID', 'STUDY_ID_MISSING')

    if (DEBUG_LEVEL >= DEBUG_LOW):
        print("*** index(): PROLIFIC_PID: " + request.session['PROLIFIC_PID'] + " SESSION_ID: " + request.session['SESSION_ID'] + " STUDY_ID: " + request.session['STUDY_ID'] + " ***")

    return render(request, 'speech/index.html')

########################################
def informedConsentDocument(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** informedConsentDocument() called  ***")

    signingStatus = "Not signed"

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** informedConsentDocument(): method was POST  ***")
        # create a form instance and populate it with data from the request:
        form = InformedConsentDocumentForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** informedConsentDocument(): form was valid  ***")
            # process the data in form.cleaned_data as required
            num_practice_trials = NUM_PRACTICE_CONGRUENT + NUM_PRACTICE_INCONGRUENT + NUM_PRACTICE_NEUTRAL
            totalTrials = num_practice_trials + NUM_CONGRUENT + NUM_INCONGRUENT + NUM_NEUTRAL

            # Generate current session
            currSubjectNumStr = str(len(StroopSession.objects.all()))
            currSession = StroopSession(
                subject_id = "test" + currSubjectNumStr.zfill(MAX_NUM_SUBJECT_DIGITS),

                prolific_pid = request.session['PROLIFIC_PID'],
                session_id = request.session['SESSION_ID'],
                study_id = request.session['STUDY_ID'],

                num_practice_trials = num_practice_trials,
                num_trials_before_break = NUM_TRIALS_BEFORE_BREAK,
                break_duration_sec = BREAK_DURATION_SEC,
                num_trials = totalTrials,
                valid = False,
                # completion_code = COMPLETION_CODE_PREFIX + currSubjectNumStr,

                informed_consent_signed = True,
                # informed_consent_signature = form.cleaned_data['informed_consent_signature'],
                informed_consent_signed_date_time = datetime.now(),
                # informed_consent_signed_date_time = form.cleaned_data['informed_consent_signed_date_time'],
            )
            currSession.save()

            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** Parsed currSession data is: " + str(currSession))

            # Save session info
            request.session['currSessionPK'] = currSession.subject_id
            # request.session['completionCode'] = currSession.completion_code
            request.session.modified = True

            #### DEBUG ###
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("[" + str(currSession.subject_id) + "] informedConsentDocument(): Received POST and populated StroopSession:" + str(form))

            # redirect to a new URL:
            return HttpResponseRedirect(reverse('speech:healthAndDemographicsForm'))

        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("*** informedConsentDocument(): form was NOT valid  ***")

    # if a GET (or any other method) we'll create a blank form
    else:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** informedConsentDocument(): method was not POST  ***")
        # form = InformedConsentDocumentForm(initial={'informed_consent_signed_date_time': datetime.now()})
        form = InformedConsentDocumentForm()

    # First, check if they had just started a new session
    if (request.session['newSession'] == True):
        request.session['newSession'] = False
        consentSigned = False
    elif (isInformedConsentSigned(request)):
        # Update form with the date and time signed
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        signingStatus = 'Signed ' + currSession.informed_consent_signed_date_time.strftime("%m/%d/%Y, %H:%M:%S") + ' (UTC time)'
        consentSigned = True
    else:
        consentSigned = False

    return render(request, 'speech/informedConsentDocument.html', {'form': form, 'consentSigned': consentSigned, 'signingStatus': signingStatus, 'readDurationSec':INFORMED_CONSENT_DOCUMENT_READ_DURATION_SEC})

########################################
def healthAndDemographicsForm(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** healthAndDemographicsForm() called  ***")

    # Enforce that informed consent was signed
    if (not isInformedConsentSigned(request)):
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** healthAndDemographicsForm(): ERROR: Informed consent NOT signed. Redirecting to form... ***")
        return HttpResponseRedirect(reverse('speech:informedConsentDocument'))
    else:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** healthAndDemographicsForm(): PASSED: Informed consent signed. Continuing... ***")

    try:
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** healthAndDemographicsForm(): method was POST  ***")
            # create a form instance and populate it with data from the request:
            # Switch based on SESSION_ID
            if (request.session['SESSION_ID'] == 'SUBJECT_POOL'):
                form = HealthAndDemographicsFormWithAge(request.POST)
            else:
                form = HealthAndDemographicsForm(request.POST)

            # check whether it's valid:
            if form.is_valid():
                if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                    print("*** healthAndDemographicsForm(): form was valid  ***")
                # process the data in form.cleaned_data as requiredSUBJECT_POOL
                currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])

                #### DEBUG ###
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("[" + str(currSession.subject_id) + "] healthAndDemographicsForm(): Received POST and populated StroopSession:" + str(form))

                # currSession.birthdate = form.cleaned_data['birthdate']
                # currSession.gender = form.cleaned_data['gender']

                # Subject Pool ONLY field
                if (request.session['SESSION_ID'] == 'SUBJECT_POOL'):
                    currSession.age = form.cleaned_data['age']

                currSession.years_of_education = form.cleaned_data['years_of_education']
                # currSession.dominant_hand = form.cleaned_data['dominant_hand']
                # currSession.alert_time_of_day = form.cleaned_data['alert_time_of_day']
                currSession.racial_categories = form.cleaned_data['racial_categories']
                currSession.is_latino = form.cleaned_data['is_latino']
                # currSession.is_english_first_language = form.cleaned_data['is_english_first_language']
                # currSession.first_language = form.cleaned_data['first_language']
                # currSession.learn_english_before_age_5 = form.cleaned_data['learn_english_before_age_5']
                # currSession.age_learned_english = form.cleaned_data['age_learned_english']
                currSession.household_income = form.cleaned_data['household_income']
                currSession.computer_os = form.cleaned_data['computer_os']
                currSession.computer_screen_width_px = form.cleaned_data['computer_screen_width_px']
                currSession.computer_screen_height_px = form.cleaned_data['computer_screen_height_px']
                currSession.computer_type = form.cleaned_data['computer_type']
                currSession.microphone_type = form.cleaned_data['microphone_type']
                currSession.hours_on_current_computer = form.cleaned_data['hours_on_current_computer']
                currSession.additional_comments = form.cleaned_data['additional_comments']

                currSession.save()

                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** Parsed currSession data is: " + str(currSession))

                # redirect to a new URL:
                return HttpResponseRedirect(reverse('speech:practice'))

            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("*** healthAndDemographicsForm(): form was NOT valid  ***")

        # if a GET (or any other method) we'll create a blank form
        else:
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** healthAndDemographicsForm(): method was not POST  ***")

            # Switch based on SESSION_ID
            if (request.session['SESSION_ID'] == 'SUBJECT_POOL'):
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** healthAndDemographicsForm(): SESSION_ID: " + request.session['SESSION_ID'] + "Creating new HealthAndDemographicsFormWithAge  ***")
                form = HealthAndDemographicsFormWithAge()
            else:
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** healthAndDemographicsForm(): SESSION_ID: " + request.session['SESSION_ID'] + "Creating new HealthAndDemographicsForm  ***")
                form = HealthAndDemographicsForm()

        return render(request, 'speech/healthAndDemographicsForm.html', {'form': form})
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** healthAndDemographicsForm(): Error occurred. Redirecting to error page  ***")
        return HttpResponseRedirect(reverse('speech:error'))

########################################
def practice(request):
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] practice() called & returning  ***")

        # Enforce that informed consent was signed
        if (not isInformedConsentSigned(request)):
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** practice(): ERROR: Informed consent NOT signed. Redirecting to form... ***")
            return HttpResponseRedirect(reverse('speech:informedConsentDocument'))
        else:
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** practice(): PASSED: Informed consent signed. Continuing... ***")

    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** practice() called. ERROR retrieving current session. Going to error page. ***")
        return HttpResponseRedirect(reverse('speech:error'))

    return render(request, 'speech/practice.html')

########################################
def practiceRetry(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** practiceRetry() called & returning  ***")
    return render(request, 'speech/practiceRetry.html')

########################################
def practiceComplete(request):
    # Enforce that informed consent was signed
    if (not isInformedConsentSigned(request)):
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** practice(): ERROR: Informed consent NOT signed. Redirecting to form... ***")
        return HttpResponseRedirect(reverse('speech:informedConsentDocument'))
    else:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** practice(): PASSED: Informed consent signed. Continuing... ***")

    # Retrieve current session
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] practiceComplete() called & returning  ***")

        # Increment number of practice sessions completed
        if (currSession.task_in_progress):
            # Mark task as no longer in progress so we don't double count
            currSession.task_in_progress = False
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("[" + str(currSession.subject_id) + "] practiceComplete(): Marked session.task_in_progress = False ***")

            currSession.num_practice_sessions_completed = currSession.num_practice_sessions_completed + 1
            currSession.save()
            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("[" + str(currSession.subject_id) + "] practiceComplete(): Incremented number of practice sessions to " + str(currSession.num_practice_sessions_completed) + "  ***")

        # Calculate practice session accuracy
        numCorrect = 0
        for trial_num in range(currSession.num_practice_trials):
            # Grab trial
            trial = currSession.trial_set.get(pk=genUUID(currSession.subject_id, trial_num))

            # Count if correct
            if (trial.ink_color == trial.response.split(' ', 1)[0]):
                numCorrect += 1

        accuracy = numCorrect / currSession.num_practice_trials
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] practiceComplete(): Accuracy of practice round was: " + str(accuracy) + "  ***")

        # Switch based on accuracy or number of times practiced
        if (accuracy >= PRACTICE_MIN_ACCURACY_THRESHOLD or currSession.num_practice_sessions_completed >= 3):
            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("[" + str(currSession.subject_id) + "] practiceComplete(): Accuracy >= " + str(PRACTICE_MIN_ACCURACY_THRESHOLD) + " or completed 3 practice sessions. Moving to practiceComplete  ***")
            return render(request, 'speech/practiceComplete.html')
        else:
            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("[" + str(currSession.subject_id) + "] practiceComplete(): Accuracy < " + str(PRACTICE_MIN_ACCURACY_THRESHOLD) + ". Moving to practiceRetry  ***")
            return render(request, 'speech/practiceRetry.html', {'percent_required': str(int(PRACTICE_MIN_ACCURACY_THRESHOLD * 100))})
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** practiceComplete() called. ERROR retrieving current session. Going to error page. ***")
        return HttpResponseRedirect(reverse('speech:error'))

########################################
def practiceCompleteAck(request):
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] practiceCompleteAck() called & returning  ***")

        request.session['practiceCompleteAcked'] = True
        request.session.modified = True

        return HttpResponseRedirect(reverse('speech:task'))
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** practiceCompleteAck() error. Going to error page ***")
        return HttpResponseRedirect(reverse('speech:error'))

########################################
def initialize(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** initialize() called & returning  ***")

    # Generate trials
    # Requirements:
    # Source: http://128.252.27.201/coglab/publications/Balota,Tse,Hutchinson,Spieler,Duchek,Morris%202010%20psych%20&%20aging.pdf
    # See Materials and Procedures:
    # Each block of trials contained 36 congruent, 36 incongruent, and 32
    # neutral trials. In the congruent condition, each of the four color names
    # appeared nine times in its corresponding color. In the incongruent
    # condition, each of the four color names appeared three times in each of
    # the three nonmatching colors. In the neutral trials, each of the four
    # neutral words appeared twice in each of the four colors.

    num_practice_trials = NUM_PRACTICE_CONGRUENT + NUM_PRACTICE_INCONGRUENT + NUM_PRACTICE_NEUTRAL
    totalTrials = num_practice_trials + NUM_CONGRUENT + NUM_INCONGRUENT + NUM_NEUTRAL

    try:
        # Grab current session
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])

        ### Generate practice trials
        practiceTrials = genTrials(NUM_PRACTICE_CONGRUENT, NUM_PRACTICE_INCONGRUENT, NUM_PRACTICE_NEUTRAL)

        ### Generate real trials
        realTrials = genTrials(NUM_CONGRUENT, NUM_INCONGRUENT, NUM_NEUTRAL)

        trialArray = practiceTrials + realTrials
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] initialize(): Shuffled trials: " + str(trialArray))
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** initialize(): Error generating trials. Going to error page.")
        return HttpResponseRedirect(reverse('speech:error'))

    # Separating this to another try-except block so that errors with create result in jumping directly to the task
    try:
        # Copy trials
        for i in range(0, totalTrials):
            currUUID = genUUID(currSession.subject_id, i)

            try:
                # If trial already exists, change its values
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** initialize(): Checking if trial already exists")
                trial = currSession.trial_set.get(pk=currUUID)
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** initialize(): trial exists. modifying it")
                trial.trial_type = trialArray[i][0]
                trial.is_practice = (i < currSession.num_practice_trials)
                trial.has_hint = (i < NUM_PRACTICE_WITH_HINTS)
                trial.trial_date_time = datetime.min
                trial.trial_number = i
                trial.RT = 0
                trial.response = ''
                trial.confidence = 0
                trial.ink_color = trialArray[i][1]
                trial.word = trialArray[i][2]
                trial.save()
                if i == 0:
                    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                        print("*** initialize(): trials already exist - being modified and saved.")
            except:
                # Else, if new trial, create it
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** initialize(): trial DOES NOT exist. creating it")
                currSession.trial_set.create(
                    uuid = currUUID,
                    trial_type = trialArray[i][0],
                    is_practice = (i < currSession.num_practice_trials),
                    has_hint = (i < NUM_PRACTICE_WITH_HINTS),
                    trial_date_time = datetime.min,
                    trial_number = i,
                    RT = 0,
                    response = '',
                    confidence = 0,
                    ink_color = trialArray[i][1],
                    word = trialArray[i][2],
                )
                if i == 0:
                    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                        print("*** initialize(): trials don't exit and are being created.")
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("[" + str(currSession.subject_id) + "] initialize(): Error creating trials. Proceeding with what was already in the database.")

    try:
        # Save session info
        request.session['currTrialNum'] = 0
        request.session['practiceCompleteAcked'] = False
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** initialize(): Error updating session currTrialNum and practiceCompleteAcked. Going to error page.")
        return HttpResponseRedirect(reverse('speech:error'))

    # Redirect us to the task page
    return HttpResponseRedirect(reverse('speech:task'))

########################################
def task(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("*** task() called  ***")

    # Enforce that informed consent was signed
    if (not isInformedConsentSigned(request)):
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("*** practice(): ERROR: Informed consent NOT signed. Redirecting to form... ***")
        return HttpResponseRedirect(reverse('speech:informedConsentDocument'))
    else:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** practice(): PASSED: Informed consent signed. Continuing... ***")

    # Grab session and populate task
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])

        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] task(): currTrialNum:" + str(request.session['currTrialNum']) + " num_practice:" + str(currSession.num_practice_trials) + " practiceCompleteAcked:" + str(request.session['practiceCompleteAcked']))
        if (request.session['currTrialNum'] == currSession.num_practice_trials and not request.session['practiceCompleteAcked']):
            # End of practice detected
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("[" + str(currSession.subject_id) + "] task(): This was the end of practice. Moving to practice complete ***")
            return HttpResponseRedirect(reverse('speech:practiceComplete'))
        if (request.session['currTrialNum'] == currSession.num_trials):
            # End of session detected
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("[" + str(currSession.subject_id) + "] task(): This was the last trial. Moving to postTaskSurvey ***")
            return HttpResponseRedirect(reverse('speech:postTaskSurvey'))
        else:
            # Proceed to next trial
            subject_id = request.session['currSessionPK']
            session = get_object_or_404(StroopSession, pk=subject_id)

            # Generate trial list
            # Initialize with start character
            trialList = "["

            for trial_num in range(session.num_trials):
                # Grab trial
                trial = session.trial_set.get(pk=genUUID(subject_id, trial_num))

                # Prefix data with bracket
                trialList = trialList + "["

                # Add current trial's data as ["ink_color", "word", is_practice, has_hint]
                trialList = trialList + "Color." + trial.ink_color + ", " \
                            + "Word." + trial.word + ", " \
                            + str(1 if trial.is_practice else 0) + ", " \
                            + str(1 if trial.has_hint else 0)

                # Postfix data with bracket to end array
                trialList = trialList + "]"

                # Add comma if there will be another trial following
                if trial_num < session.num_trials - 1:
                    trialList = trialList + ","

            # Append trialList end character
            trialList = trialList + "]"

            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("[" + str(currSession.subject_id) + "] task(): pk=" + str(request.session['currSessionPK']) + " currTrialNum=" + str(trial_num) + "   subject_id=" + str(subject_id) + " trial=" + str(trial) + "***")

            # Mark task as in progress
            session.task_in_progress = True
            session.save()
            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("[" + str(currSession.subject_id) + "] task(): Marked session.task_in_progress = True ***")

            return render(request, 'speech/task.html', {'trialList' : trialList, 'numPractice' : session.num_practice_trials, 'numPracticeWithHints': NUM_PRACTICE_WITH_HINTS, 'startingTrialNum': request.session['currTrialNum'], 'breakTrialNum': NUM_TRIALS_BEFORE_BREAK, 'breakDurationSec': BREAK_DURATION_SEC})
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** task(): Error has occurred. Redirecting to error page ***")
        return render(request, 'speech/error.html')

########################################
@csrf_exempt
def taskComplete(request):
    if (DEBUG_LEVEL >= DEBUG_HIGH):
        print("*** taskComplete() called  ***")
    try:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** Received RT: " + request.POST['RT'] + " with response: " + request.POST['response'] + " with confidence: " + request.POST['confidence'] + " ***")

        currTrialNum = request.POST['trialNum'] #request.session['currTrialNum']

        # Use the session + POST data here to save it to the database
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        currTrial = currSession.trial_set.get(pk=genUUID(currSession.subject_id, currTrialNum))
        currTrial.trial_date_time = timezone.now()
        currTrial.RT = request.POST['RT']
        currTrial.response = request.POST['response'].upper()
        currTrial.confidence = request.POST['confidence']
        currTrial.save()

        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("[" + str(currSession.subject_id) + "] taskComplete(): Trial num:" + str(request.POST['trialNum']) + " trial: " + str(currTrial) + " Received RT: " + request.POST['RT'] + " with response: " + request.POST['response'] + " with confidence: " + request.POST['confidence'] + " ***")

        # Increment trial number
        request.session['currTrialNum'] = int(currTrialNum) + 1
        request.session.modified = True

    except (KeyError):
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** taskComplete(): KeyError occurred ***")
        return HttpResponseRedirect(reverse('speech:error'))
    else:
        if (DEBUG_LEVEL >= DEBUG_HIGH):
            print("*** taskComplete(): else condition ***")
        return HttpResponseRedirect(reverse('speech:task'))

########################################
def postTaskSurvey(request):
    if (DEBUG_LEVEL >= DEBUG_MEDIUM): print("*** postTaskSurvey() called  ***")

    try:
        # Mark task as not in progress
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        currSession.task_in_progress = False
        currSession.save()

        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** postTaskSurvey(): method was POST  ***")
            # create a form instance and populate it with data from the request:
            form = PostTaskForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("*** postTaskSurvey(): form was valid  ***")

                try:
                    # Save data
                    # currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
                    currSession.done_stroop_previously = 'USER_' + form.cleaned_data['done_stroop_previously']
                    currSession.post_task_comments = form.cleaned_data['post_task_comments']
                    currSession.save()

                    #### DEBUG ###
                    if (DEBUG_LEVEL >= DEBUG_HIGH):
                        print("[" + str(currSession.subject_id) + "] postTaskSurvey(): Received POST and updated StroopSession:" + str(form))

                    # redirect to a new URL:
                    return HttpResponseRedirect(reverse('speech:success'))
                except:
                    return HttpResponseRedirect(reverse('speech:error'))

            if (DEBUG_LEVEL >= DEBUG_MEDIUM): print("*** postTaskSurvey(): form was NOT valid  ***")

        # if a GET (or any other method) we'll create a blank form
        else:
            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** postTaskSurvey(): method was not POST  ***")
            form = PostTaskForm()

        return render(request, 'speech/postTaskSurvey.html', {'form': form})
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW): print("*** postTaskSurvey(): Error occurred. Redirecting to error page  ***")
        return HttpResponseRedirect(reverse('speech:error'))

########################################
def success(request):
    if (DEBUG_LEVEL >= DEBUG_HIGH): print("*** success() called ***")

    # Mark session valid in database
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        currSession.valid = True
        currSession.save()

        # # Display the completion code
        # try:
        #     completionCode = request.session['completionCode']
        # except:
        #     completionCode = "ERROR: Please complete the task first!"

        # Send email notification
        # if (settings.SPEECHSTROOP_SEND_EMAIL):
        #     try:
        #         send_mail(
        #             '[speechstroop] Task completed by ' + str(request.session['currSessionPK']),
        #             'View results at Go to  https://www.pythonanywhere.com/user/coglab/consoles/14763158/ and log in.\n' +
        #             'Type the following commands into the terminal:\n' +
        #             'cd ~/speechstroop\n' +
        #             './dumpMysqlDb.sh\n' +
        #             'Then go to https://www.pythonanywhere.com/user/coglab/files/home/coglab/speechstroop/DatabaseDump and download the latest 2 CSVs.',
        #             settings.SPEECH_EMAIL_SENDER,
        #             settings.SPEECHSTROOP_EMAIL_RECIPIENTS,
        #             fail_silently=False,
        #         )
        #     except:
        #         print("[" + str(currSession.subject_id) + "] success(): ERROR sending email  ***")

        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] success() returning  ***")
        # return render(request, 'speech/success.html', {'completionCode': completionCode})

        # Determine completion link - Switch based on SESSION_ID
        if (request.session['SESSION_ID'] == 'SUBJECT_POOL'):
            completionLink = SUBJECT_POOL_COMPLETION_LINK + request.session['PROLIFIC_PID']
            testingSite = "SONA"

            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** success(): Setting completion link to subject pool: " + completionLink + " ***")
        else:
            completionLink = PROLIFIC_COMPLETION_LINK
            testingSite = "Prolific"

            if (DEBUG_LEVEL >= DEBUG_HIGH):
                print("*** success(): Setting completion link to prolific ***")

        return render(request, 'speech/success.html', {'prolificCompletionLink': completionLink, 'testingSite': testingSite})
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** success(): Error attempting to retrieve currSession! ***")
        return render(request, 'speech/error.html')

########################################
def error(request):
    try:
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("[" + str(currSession.subject_id) + "] error() called & returning  ***")
    except:
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("*** error() called & returning  ***")
    return render(request, 'speech/error.html')


########################################
######### Helper Functions #############
########################################
def genUUID(subject_id, trial_num):
    return subject_id + "-" + str(trial_num).zfill(MAX_NUM_TRIAL_DIGITS)

def genTrials(numCongruent, numIncongruent, numNeutral):
    # Allocate array for generating trials
    # Format is [(congruence, ink_color, word)]
    trialArray = []

    if (DEBUG_LEVEL >= DEBUG_MEDIUM):
        print("genTrials() called with: " + str(numCongruent) + ", " + str(numIncongruent) + ", " + str(numNeutral))

    # Generate congruent trials
    for i in range(0, numCongruent):
        colorNum = randint(0, len(Trial.COLOR_CHOICES) - 1)
        trialArray.append((Trial.CONGRUENT, Trial.COLOR_CHOICES[colorNum][0], Trial.WORD_CHOICES[colorNum][0]))

    # Generate incongruent trials
    for i in range(0, numIncongruent):
        colorNum = randint(0, len(Trial.COLOR_CHOICES) - 1)
        wordNum = colorNum
        while wordNum == colorNum:
            wordNum = randint(0, len(Trial.COLOR_CHOICES) - 1)

        trialArray.append((Trial.INCONGRUENT, Trial.COLOR_CHOICES[colorNum][0], Trial.WORD_CHOICES[wordNum][0]))

    # Generate neutral trials
    for i in range(0, numNeutral):
        colorNum = randint(0, len(Trial.COLOR_CHOICES) - 1)
        wordNum = randint(len(Trial.COLOR_CHOICES), len(Trial.WORD_CHOICES) - 1)

        trialArray.append((Trial.NEUTRAL, Trial.COLOR_CHOICES[colorNum][0], Trial.WORD_CHOICES[wordNum][0]))

    # Shuffle the deck
    shuffle(trialArray)
    makeTrialArrayValid(trialArray)

    if (DEBUG_LEVEL >= DEBUG_HIGH):
        print("genTrials(): valid trialArray, returning: " + str(trialArray))
    return trialArray

# Function to check whether the provided trial array is valid. An array is
# considered valid if:
#   1) There are no more than 3 consecutive trials with the same word or color
#   2) There are no consecutive identical trials (same word and same ink color)
def makeTrialArrayValid(trialArray):
    if (DEBUG_LEVEL >= DEBUG_HIGH):
        print("makeTrialArrayValid(): Checking trialArray: " + str(trialArray))

    # Initialize as false to force it to run
    trialArrayValid = False

    while (not trialArrayValid):
        # Every iteration, start off as true
        trialArrayValid = True

        # Condition 1: Search for 3 consecutive trials with the same word or color
        # Prereq: Must have at least 3 elements in the array!
        if (len(trialArray) >= 3):
            # Start at 2, look at the previous 3 elements
            for i in range(2, len(trialArray)):
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("makeTrialArrayValid(): Condition 1: Checking index " + str(i))

                currentThree = trialArray[(i - 2):(i + 1)]
                if (all(a[INK_COLOR] == currentThree[0][INK_COLOR] for a in currentThree) or all(a[WORD] == currentThree[0][WORD] for a in currentThree)):
                    if (DEBUG_LEVEL >= DEBUG_HIGH):
                        print("makeTrialArrayValid(): Condition 1: Failed on index " + str(i) + " with: " + str(currentThree))

                    # Swap this one with a random position
                    swap(trialArray, i - 1, randint(0, len(trialArray) - 1))
                    trialArrayValid = False

        # Condition 2: Check for consecutive identical trials
        if (len(trialArray) >= 2):
            # Start at 1, look at the previous 2 elements
            for i in range(1, len(trialArray)):
                if (DEBUG_LEVEL >= DEBUG_HIGH):
                    print("makeTrialArrayValid(): Condition 2: Checking index " + str(i))
                if ((trialArray[i - 1][INK_COLOR] == trialArray[i][INK_COLOR]) and (trialArray[i - 1][WORD] == trialArray[i][WORD])):
                    if (DEBUG_LEVEL >= DEBUG_HIGH):
                        print("makeTrialArrayValid(): Condition 2: Failed on index " + str(i) + " with: " + str(trialArray[i - 1]) + " and " + str(trialArray[i]))
                    swap(trialArray, i, randint(0, len(trialArray) - 1))
                    trialArrayValid = False

    # If we make it to the end, the array is valid
    if (DEBUG_LEVEL >= DEBUG_HIGH):
        print("makeTrialArrayValid(): trialArray passed!")

# Function to swap positions of two indices in given array
def swap(a, idx1, idx2):
    if (idx1 < 0 or idx2 < 0 or idx1 >= len(a) or idx2 >= len(a)):
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("swap(" + str(idx1) + "," + str(idx2) + "): ERROR: An index is out of range!")
        return
    temp = a[idx1]
    a[idx1] = a[idx2]
    a[idx2] = temp
    if (DEBUG_LEVEL >= DEBUG_HIGH):
        print("swap(" + str(idx1) + "," + str(idx2) + "): Success!")


# Function to check that informed consent is signed
# Returns true if informed consent is signed. Returns false if informed consent
#   not signed or session cannot be found in the database.
def isInformedConsentSigned(request):
    try:
        # First, check if this is a new session (someone had revisited index)
        if request.session['newSession'] == True:
            if (DEBUG_LEVEL >= DEBUG_MEDIUM):
                print("*** isInformedConsentSigned(): newSession marked true. Returning false ***")
            return False

        # Now check database
        currSession = StroopSession.objects.get(pk=request.session['currSessionPK'])
        if (DEBUG_LEVEL >= DEBUG_MEDIUM):
            print("*** isInformedConsentSigned(): StroopSession retrieved. informed_consent_signed=" + str(currSession.informed_consent_signed) + " ***")
        return currSession.informed_consent_signed
    except:
        if (DEBUG_LEVEL >= DEBUG_LOW):
            print("*** isInformedConsentSigned(): Error has occurred while attempting to retrieve StroopSession! Returning false ***")
        return False
