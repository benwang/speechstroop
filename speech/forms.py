from django.forms import ModelForm, SelectDateWidget, CheckboxSelectMultiple, forms
from .models import StroopSession
from datetime import datetime

# # Limits for birthdate years
# MIN_AGE = 18
# MAX_AGE = 100
#
# currYear = datetime.now().year
# MIN_YEAR = currYear - MAX_AGE - 1
# MAX_YEAR = currYear - MIN_AGE
#
# GENDER_CHOICES=[
#     ('male', 'Male'),
#     ('female', 'Female'),
#     ('other', 'Other')
# ]

class InformedConsentDocumentForm(ModelForm):
    # Function below to set signed date as readonly
    # def __init__(self, *args, **kwargs):
    #    super(InformedConsentDocumentForm, self).__init__(*args, **kwargs)
    #    self.fields['informed_consent_signed_date_time'].widget.attrs['readonly'] = True

    class Meta:
        model = StroopSession
        # fields = ['informed_consent_signature', 'informed_consent_signed_date_time']
        fields = []

class HealthAndDemographicsForm(ModelForm):
    class Meta:
        model = StroopSession
        fields = ['years_of_education', 'racial_categories', 'is_latino', 'household_income', 'computer_type', 'microphone_type', 'hours_on_current_computer', 'computer_os', 'computer_screen_width_px', 'computer_screen_height_px', 'additional_comments']

class HealthAndDemographicsFormWithAge(ModelForm):
    class Meta:
        model = StroopSession
        fields = ['age', 'years_of_education', 'racial_categories', 'is_latino', 'household_income', 'computer_type', 'microphone_type', 'hours_on_current_computer', 'computer_os', 'computer_screen_width_px', 'computer_screen_height_px', 'additional_comments']
        # fields = ['birthdate', 'gender', 'years_of_education', 'dominant_hand', 'alert_time_of_day', 'racial_categories', 'is_latino', 'is_english_first_language', 'first_language', 'learn_english_before_age_5', 'age_learned_english', 'additional_comments']
        # widgets = {
        #     'birthdate': SelectDateWidget(years = list(range(MAX_YEAR, MIN_YEAR, -1))),
        #     # 'racial_categories': CheckboxSelectMultiple
        # }
        # fields = '__all__'
    # birthdate = forms.DateField(label='Birthdate')
    # gender = forms.CharField(label='Gender', widget=forms.Select(choices=GENDER_CHOICES))

    # def clean(self):
    #     print("************ clean() called!")
    #
    #     # First language
    #     is_english_first_language = self.cleaned_data.get('is_english_first_language')
    #     first_language = self.cleaned_data.get('first_language')
    #
    #     # Age learned english
    #     learn_english_before_age_5 = self.cleaned_data.get('learn_english_before_age_5')
    #     age_learned_english = self.cleaned_data.get('age_learned_english')
    #
    #     if is_english_first_language == StroopSession.NO:
    #         print("***clean(): english not first language. Checking first language")
    #         if first_language == '':
    #             print("***clean(): english not first language. first language was empty. Error.")
    #             msg = forms.ValidationError("The following field is required:")
    #             self.add_error('first_language', msg)
    #     else:
    #         # Keep the database consistent. The user may have
    #         # submitted a shipping_destination even if shipping
    #         # was not selected
    #         self.cleaned_data['first_language'] = ''
    #
    #     if learn_english_before_age_5 == StroopSession.NO:
    #         print("***clean(): learned english after age 5. Checking age")
    #         if age_learned_english == 0:
    #             print("***clean(): english not first language. age learned english == 0. Error")
    #             msg = forms.ValidationError("The following field is required:")
    #             self.add_error('age_learned_english', msg)
    #     else:
    #         # Keep the database consistent. The user may have
    #         # submitted a shipping_destination even if shipping
    #         # was not selected
    #         self.cleaned_data['age_learned_english'] = 0
    #
    #     return self.cleaned_data

class PostTaskForm(ModelForm):
    class Meta:
        model = StroopSession
        fields = ['done_stroop_previously', 'post_task_comments']
