#!/bin/bash

# dumpMysqlDb.sh
# Script used to dump contents of Stroop MySQL database to CSVs for analysis.
# Calls both db-export scripts (sessions and trials) found in the scripts directory.

# Set the PythonAnywhere database user (benwang or coglab)
databaseUser=coglab

dateString=$(date +%Y%m%d-%H%M%S)
dumpPath=DatabaseDump
scriptsDir=scripts

echo "Dumping database to ./databaseDump folder from PythonAnywhere user:" $databaseUser

if [ ! -d $dumpPath ]; then
    mkdir $dumpPath
fi
cd $dumpPath
mysqldump -u $databaseUser -h $databaseUser.mysql.pythonanywhere-services.com $databaseUser'$default'  > db-backup-$dateString.sql

/bin/bash ../$scriptsDir/exportCSV-trials.sh db-backup-$dateString.sql db-export-$dateString-trials.csv
/bin/bash ../$scriptsDir/exportCSV-sessions.sh db-backup-$dateString.sql db-export-$dateString-sessions.csv

echo "Dump complete."
